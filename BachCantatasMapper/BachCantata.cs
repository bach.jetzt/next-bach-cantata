using System;

namespace BachJetzt
{
    public class BachCantata
    {
        public string Name { get; set; }

        public string Bwv { get; set; }

        public Uri SpotifyUrl { get; set; }

        public CantataStatus Status { get; set; }

        public override string ToString()
        {
            return $"{nameof(Name)}: {Name}, {nameof(Bwv)}: {Bwv}";
        }
    }
}