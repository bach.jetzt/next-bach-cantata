namespace BachJetzt
{
    /// <summary>
    /// Not all Cantatas are still preserved and have a recording on spotify. However, they are documented. 
    /// </summary>
    public enum CantataStatus
    {
        /// <summary>
        /// Existing and available on spotify
        /// </summary>
        Accessible,
        /// <summary>
        /// Not preserved
        /// </summary>
        NotAvailable,
        /// <summary>
        /// Preserved only in fragments
        /// </summary>
        Fragment,
        /// <summary>
        /// Not preserved completely
        /// </summary>
        Incomplete,
    }
}