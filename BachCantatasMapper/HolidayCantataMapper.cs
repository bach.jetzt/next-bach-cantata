﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace BachJetzt
{
    public class HolidayCantataMapper
    {
        public HolidayCantataMapper()
        {
            var text = File.ReadAllText("BachCantatas.json");
            _bachCantataList = JsonConvert.DeserializeObject<Dictionary<LiturgicalHoliday, List<BachCantata>>>(text);
        }

        public IList<BachCantata> GetCantatas(LiturgicalHoliday holiday)
        {
            return _bachCantataList.TryGetValue(holiday, out var cantatasList) ? cantatasList : null;
        }

        private readonly Dictionary<LiturgicalHoliday, List<BachCantata>> _bachCantataList;
    }
}