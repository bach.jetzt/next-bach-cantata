# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.1](https://gitlab.com/bach.jetzt/next-bach-cantata/compare/1.0.0...1.0.1) (2020-03-01)


### Bug Fixes

* **nuget:** adds icon to nuget package ([0aba755](https://gitlab.com/bach.jetzt/next-bach-cantata/commit/0aba7554213279a1e6f40b233df55c006624e720))

## 1.0.0 (2020-03-01)


### Features

* **standard-version:** adds versionrc config file for versioning via standard-version ([a3a8d27](https://gitlab.com/bach.jetzt/next-bach-cantata/commit/a3a8d273717cce61872a5f11cbc17760a0fff200))


### Bug Fixes

* **BachCanatatas:** adds information on unavailable cantatas ([33ac370](https://gitlab.com/bach.jetzt/next-bach-cantata/commit/33ac37010a8ee3555033e79849aaf7c18ece8442))
* **BachCantatasMapper:** adds first Spotify URLs, addds 4th property (status) to json, translates liturigal holiday enum into english ([dfa421f](https://gitlab.com/bach.jetzt/next-bach-cantata/commit/dfa421f96a51672dd2f41ec8931323c446f112d1))
* **namespace:** adjusted all namespaces to BachJetzt ([f15d4dc](https://gitlab.com/bach.jetzt/next-bach-cantata/commit/f15d4dce0ea36cb28b917bfbc7c261cbe0eec0a7))
