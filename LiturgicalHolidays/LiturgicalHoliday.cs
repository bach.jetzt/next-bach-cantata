namespace BachJetzt
{
    public enum LiturgicalHoliday
    {
        Advent1,
        StNicholas, // Nikolaustag
        Advent2,
        Advent3,
        Advent4,
        ChristmasEve, // Heiligabend
        ChristmasDay1, // Erster Christtag
        ChristmasDay2,
        SundayAfterChristmas,
        NewYearsEve, // Altjahresabend
        NewYearsDay,
        Epiphany,
        Sunday1AfterEpiphany,
        Sunday2AfterEpiphany,
        Sunday3AfterEpiphany,
        Sunday4AfterEpiphany,
        Sunday5BeforeLent, //5. Sonntag vor der Passionszeit
        Sunday4BeforeLent,
        Septuagesimae,
        Sexagesimae,
        Estomihi,
        AshWednesday,
        Invocavit,
        Reminiscere,
        Oculi,
        Laetare,
        Judica,
        Palmarum,
        MaundyThursday, // Gründonnerstag
        GoodFriday, // Karfreitag
        HolySaturday, // Karsamstag
        EasterSunday,
        EasterMonday,
        EasterTuesday,
        Quasimodogeniti,
        MisericordiasDomini,
        Jubilate,
        Kantate,
        Rogate,
        Ascension,
        Exaudi,
        PentecostSunday, //Pfingstsonntag
        PentecostMonday,
        PentecostTuesday,
        Trinity,
        Sunday1AfterTrinity,
        Sunday2AfterTrinity,
        StJohn, // Johannisfest
        Sunday3AfterTrinity,
        Sunday4AfterTrinity,
        Sunday5AfterTrinity,
        Sunday6AfterTrinity,
        Sunday7AfterTrinity,
        Sunday8AfterTrinity,
        Sunday9AfterTrinity,
        Sunday10AfterTrinity,// Israelsonntag
        Sunday11AfterTrinity,
        Sunday12AfterTrinity,
        Sunday13AfterTrinity,
        Sunday14AfterTrinity,
        Sunday15AfterTrinity,
        Sunday16AfterTrinity,
        StMichael, // Michaelisfest
        Thanksgiving, // Erntedank
        Sunday17AfterTrinity,
        Sunday18AfterTrinity,
        Sunday19AfterTrinity,
        Sunday20AfterTrinity,
        Reformation,
        Sunday21AfterTrinity,
        Sunday22AfterTrinity,
        StMartin, // Martinstag
        Sunday23AfterTrinity,
        Sunday24AfterTrinity,
        RepentanceAndPrayer, // Buß- und Bettag
        EternitySunday, //Ewigkeitssonntag
        Annunciation, // Mariä Verkündigung
        Presentation, // Mariä Reinigung
        MaryVisitation, // Mariä Heimsuchung
        
    }
}