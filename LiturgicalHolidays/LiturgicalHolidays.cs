﻿using System;
using Holidays.Moveable;

namespace BachJetzt
{
    public class LiturgicalHolidays
    {
         public static LiturgicalHoliday? GetLiturgicalHoliday(DateTime day)
        {
            var startOfCurrentLiturgicalYear = GetStartOfLiturgicalYear(day);
            if (startOfCurrentLiturgicalYear > day)
            {
                startOfCurrentLiturgicalYear = GetStartOfLiturgicalYear(day.AddYears(-1));
            }

            var liturgicalYearSection = GetLiturgicalYearSection(startOfCurrentLiturgicalYear, day);
            
            return liturgicalYearSection switch
            {
                LiturgicalYearSection.Christmas => GetChristmasHoliday(startOfCurrentLiturgicalYear, day),
                LiturgicalYearSection.Easter => GetEasterHoliday(startOfCurrentLiturgicalYear, day),
                LiturgicalYearSection.Trinity => GetTrinityHoliday(startOfCurrentLiturgicalYear, day),
                _ => throw new ArgumentOutOfRangeException()
            };
        }


        private static LiturgicalHoliday? GetChristmasHoliday(in DateTime startOfCurrentLiturgicalYear, in DateTime day)
        {
            var currentYear = day.Year;
            var epiphany = new DateTime(currentYear, 1, 6);
            var sundayAfterEpiphany = GetNextWeekday(epiphany, DayOfWeek.Sunday);
            if (day.Date.Equals(startOfCurrentLiturgicalYear.Date))
            {
                return LiturgicalHoliday.Advent1;
            }

            if (day.Month == 12 && day.Day == 6)
            {
                return LiturgicalHoliday.StNicholas;
            }

            if (day.Date.Equals(startOfCurrentLiturgicalYear.AddDays(7).Date))
            {
                return LiturgicalHoliday.Advent2;
            }

            if (day.Date.Equals(startOfCurrentLiturgicalYear.AddDays(14).Date))
            {
                return LiturgicalHoliday.Advent3;
            }

            if (day.Month == 12 && day.Day == 24)
            {
                return LiturgicalHoliday.ChristmasEve;
            }

            if (day.Date.Equals(startOfCurrentLiturgicalYear.AddDays(21).Date))
            {
                return LiturgicalHoliday.Advent4;
            }

            if (day.Month == 12 && day.Day == 25)
            {
                return LiturgicalHoliday.ChristmasDay1;
            }

            if (day.Month == 12 && day.Day == 26)
            {
                return LiturgicalHoliday.ChristmasDay2;
            }

            if (day.Month == 12 && day.Day == 31)
            {
                return LiturgicalHoliday.NewYearsEve;
            }

            if (day.Month == 1 && day.Day == 1)
            {
                return LiturgicalHoliday.NewYearsDay;
            }

            if (day.Date.Equals(startOfCurrentLiturgicalYear.AddDays(28).Date))
            {
                return LiturgicalHoliday.SundayAfterChristmas;
            }

            if (day.Month == 1 && day.Day == 6)
            {
                return LiturgicalHoliday.Epiphany;
            }

            if (day.Date.Equals(sundayAfterEpiphany.Date))
            {
                return LiturgicalHoliday.Sunday1AfterEpiphany;
            }

            if (day.Date.Equals(sundayAfterEpiphany.AddDays(7).Date))
            {
                return LiturgicalHoliday.Sunday2AfterEpiphany;
            }

            if (day.Date.Equals(sundayAfterEpiphany.AddDays(14).Date))
            {
                return LiturgicalHoliday.Sunday3AfterEpiphany;
            }

            if (day.Date.Equals(sundayAfterEpiphany.AddDays(21).Date))
            {
                return LiturgicalHoliday.Sunday4AfterEpiphany;
            }
            if (day.Month == 3 && day.Day == 25)
            {
                return LiturgicalHoliday.Annunciation;
            }
            if (day.Month == 2 && day.Day == 2)
            {
                return LiturgicalHoliday.Presentation;
            }
            return null;
        }

        private static LiturgicalHoliday? GetEasterHoliday(in DateTime startOfCurrentLiturgicalYear, in DateTime day)
        {
            var christianHolidaysCalendar = new ChristianHolidays(day.Year);
            var easter = christianHolidaysCalendar.Easter;

            if (day.Month == 3 && day.Day == 25)
            {
                return LiturgicalHoliday.Annunciation;
            }
            if (day.Date.Equals(easter.AddDays(-77).Date))
            {
                return LiturgicalHoliday.Sunday5BeforeLent;
            }

            if (day.Date.Equals(easter.AddDays(-70).Date))
            {
                return LiturgicalHoliday.Sunday4BeforeLent;
            }

            if (day.Date.Equals(easter.AddDays(-63).Date))
            {
                return LiturgicalHoliday.Septuagesimae;
            }

            if (day.Date.Equals(easter.AddDays(-56).Date))
            {
                return LiturgicalHoliday.Sexagesimae;
            }

            if (day.Date.Equals(easter.AddDays(-49).Date))
            {
                return LiturgicalHoliday.Estomihi;
            }

            if (day.Date.Equals(easter.AddDays(-46).Date))
            {
                return LiturgicalHoliday.AshWednesday;
            }

            if (day.Date.Equals(easter.AddDays(-42).Date))
            {
                return LiturgicalHoliday.Invocavit;
            }

            if (day.Date.Equals(easter.AddDays(-35).Date))
            {
                return LiturgicalHoliday.Reminiscere;
            }

            if (day.Date.Equals(easter.AddDays(-28).Date))
            {
                return LiturgicalHoliday.Oculi;
            }

            if (day.Date.Equals(easter.AddDays(-21).Date))
            {
                return LiturgicalHoliday.Laetare;
            }

            if (day.Date.Equals(easter.AddDays(-14).Date))
            {
                return LiturgicalHoliday.Judica;
            }

            if (day.Date.Equals(easter.AddDays(-7).Date))
            {
                return LiturgicalHoliday.Palmarum;
            }

            if (day.Date.Equals(easter.AddDays(-3).Date))
            {
                return LiturgicalHoliday.MaundyThursday;
            }

            if (day.Date.Equals(easter.AddDays(-2).Date))
            {
                return LiturgicalHoliday.GoodFriday;
            }

            if (day.Date.Equals(easter.AddDays(-1).Date))
            {
                return LiturgicalHoliday.HolySaturday;
            }

            if (day.Date.Equals(easter.Date))
            {
                return LiturgicalHoliday.EasterSunday;
            }

            if (day.Date.Equals(easter.AddDays(1).Date))
            {
                return LiturgicalHoliday.EasterMonday;
            }
            if (day.Date.Equals(easter.AddDays(2).Date))
            {
                return LiturgicalHoliday.EasterTuesday;
            }

            if (day.Date.Equals(easter.AddDays(7).Date))
            {
                return LiturgicalHoliday.Quasimodogeniti;
            }

            if (day.Date.Equals(easter.AddDays(14).Date))
            {
                return LiturgicalHoliday.MisericordiasDomini;
            }

            if (day.Date.Equals(easter.AddDays(21).Date))
            {
                return LiturgicalHoliday.Jubilate;
            }

            if (day.Date.Equals(easter.AddDays(28).Date))
            {
                return LiturgicalHoliday.Kantate;
            }

            if (day.Date.Equals(easter.AddDays(35).Date))
            {
                return LiturgicalHoliday.Rogate;
            }

            if (day.Date.Equals(easter.AddDays(42).Date))
            {
                return LiturgicalHoliday.Exaudi;
            }

            if (day.Date.Equals(easter.AddDays(39).Date))
            {
                return LiturgicalHoliday.Ascension;
            }

            if (day.Date.Equals(easter.AddDays(49).Date))
            {
                return LiturgicalHoliday.PentecostSunday;
            }

            if (day.Date.Equals(easter.AddDays(50).Date))
            {
                return LiturgicalHoliday.PentecostMonday;
            }
if (day.Date.Equals(easter.AddDays(51).Date))
            {
                return LiturgicalHoliday.PentecostTuesday;
            }

            return null;
        }

        private static LiturgicalHoliday? GetTrinityHoliday(in DateTime startOfCurrentLiturgicalYear, in DateTime day)
        {
            var christianHolidaysCalendar = new ChristianHolidays(day.Year);
            var trinity = christianHolidaysCalendar.Easter.AddDays(56);
            var startOfNextLiturgicalYear = GetStartOfLiturgicalYear(day);

            if (day.Date.Equals(trinity.Date))
            {
                return LiturgicalHoliday.Trinity;
            }

            if (day.Month == 10 && day.Day <= 7 && day.Date.Equals(GetNextWeekday(day, DayOfWeek.Sunday).Date))
            {
                return (LiturgicalHoliday.Thanksgiving);
            }

            if (day.Date.Equals(trinity.AddDays(7).Date))
            {
                return LiturgicalHoliday.Sunday1AfterTrinity;
            }

            if (day.Date.Equals(trinity.AddDays(14).Date))
            {
                return LiturgicalHoliday.Sunday2AfterTrinity;
            }

            if (day.Date.Equals(trinity.AddDays(21).Date))
            {
                return LiturgicalHoliday.Sunday3AfterTrinity;
            }

            if (day.Date.Equals(trinity.AddDays(28).Date))
            {
                return LiturgicalHoliday.Sunday4AfterTrinity;
            }

            if (day.Date.Equals(trinity.AddDays(35).Date))
            {
                return LiturgicalHoliday.Sunday5AfterTrinity;
            }

            if (day.Date.Equals(trinity.AddDays(42).Date))
            {
                return LiturgicalHoliday.Sunday6AfterTrinity;
            }

            if (day.Date.Equals(trinity.AddDays(49).Date))
            {
                return LiturgicalHoliday.Sunday7AfterTrinity;
            }

            if (day.Date.Equals(trinity.AddDays(56).Date))
            {
                return LiturgicalHoliday.Sunday8AfterTrinity;
            }

            if (day.Date.Equals(trinity.AddDays(63).Date))
            {
                return LiturgicalHoliday.Sunday9AfterTrinity;
            }

            if (day.Date.Equals(trinity.AddDays(70).Date))
            {
                return LiturgicalHoliday.Sunday10AfterTrinity;
            }

            if (day.Date.Equals(trinity.AddDays(77).Date))
            {
                return LiturgicalHoliday.Sunday11AfterTrinity;
            }

            if (day.Date.Equals(trinity.AddDays(84).Date))
            {
                return LiturgicalHoliday.Sunday12AfterTrinity;
            }

            if (day.Date.Equals(trinity.AddDays(91).Date))
            {
                return LiturgicalHoliday.Sunday13AfterTrinity;
            }

            if (day.Date.Equals(trinity.AddDays(98).Date))
            {
                return LiturgicalHoliday.Sunday14AfterTrinity;
            }

            if (day.Date.Equals(trinity.AddDays(105).Date))
            {
                return LiturgicalHoliday.Sunday15AfterTrinity;
            }

            if (day.Date.Equals(trinity.AddDays(112).Date))
            {
                return LiturgicalHoliday.Sunday16AfterTrinity;
            }

            if (day.Date.Equals(trinity.AddDays(119).Date))
            {
                return LiturgicalHoliday.Sunday17AfterTrinity;
            }

            if (day.Date.Equals(trinity.AddDays(126).Date))
            {
                return LiturgicalHoliday.Sunday18AfterTrinity;
            }

            if (day.Date.Equals(startOfNextLiturgicalYear.AddDays(-7).Date))
            {
                return LiturgicalHoliday.EternitySunday;
            }

            if (day.Date.Equals(trinity.AddDays(133).Date))
            {
                return LiturgicalHoliday.Sunday19AfterTrinity;
            }

            if (day.Date.Equals(trinity.AddDays(140).Date))
            {
                return LiturgicalHoliday.Sunday20AfterTrinity;
            }

            if (day.Date.Equals(trinity.AddDays(147).Date))
            {
                return LiturgicalHoliday.Sunday21AfterTrinity;
            }

            if (day.Date.Equals(trinity.AddDays(154).Date))
            {
                return LiturgicalHoliday.Sunday22AfterTrinity;
            }

            if (day.Date.Equals(trinity.AddDays(161).Date))
            {
                return LiturgicalHoliday.Sunday23AfterTrinity;
            }

            if (day.Date.Equals(trinity.AddDays(168).Date))
            {
                return LiturgicalHoliday.Sunday24AfterTrinity;
            }

            if (day.Month == 10 && day.Day == 31)
            {
                return LiturgicalHoliday.Reformation;
            }

            if (day.Month == 9 && day.Day == 29)
            {
                return LiturgicalHoliday.StMichael;
            }

            if (day.Month == 11 && day.Day == 11)
            {
                return LiturgicalHoliday.StMartin;
            }

            if (day.Month == 6 && day.Day == 24)
            {
                return LiturgicalHoliday.StJohn;
            }

            if (day.Month == 7 && day.Day == 2)
            {
                return LiturgicalHoliday.MaryVisitation;
            }
            
            if (day.Date.Equals(startOfNextLiturgicalYear.AddDays(-11).Date))
            {
                return LiturgicalHoliday.RepentanceAndPrayer;
            }

            return null;
        }


        private static LiturgicalYearSection GetLiturgicalYearSection(DateTime startOfCurrentLiturgicalYear,
            DateTime day)
        {
            var endOfChristmasSection = new DateTime(startOfCurrentLiturgicalYear.AddYears(1).Year, 2, 2);
            if (day >= startOfCurrentLiturgicalYear && day <= endOfChristmasSection)
            {
                return LiturgicalYearSection.Christmas;
            }

            var christianHolidaysCalendar = new ChristianHolidays(day.Year);
            return day <= christianHolidaysCalendar.Easter.AddDays(50)
                ? LiturgicalYearSection.Easter
                : LiturgicalYearSection.Trinity;
        }

        private static DateTime GetStartOfLiturgicalYear(DateTime day)
        {
            var currentYear = day.Year;
            var currentYear3011 = new DateTime(currentYear, 11, 30);
            var sundayBefore3011 = GetNextWeekday(currentYear3011.AddDays(-7), DayOfWeek.Sunday);
            var sundayAfter3011 = GetNextWeekday(currentYear3011, DayOfWeek.Sunday);
            if (currentYear3011.DayOfWeek == DayOfWeek.Sunday)
            {
                return currentYear3011;
            }

            var timeSpanToSundayBefore = currentYear3011.Subtract(sundayBefore3011);
            var timeSpanToSundayAfter = sundayAfter3011.Subtract(currentYear3011);
            return timeSpanToSundayBefore < timeSpanToSundayAfter ? sundayBefore3011 : sundayAfter3011;
        }


        private static DateTime GetNextWeekday(DateTime start, DayOfWeek day)
        {
            // The (... + 7) % 7 ensures we end up with a value in the range [0, 6]
            int daysToAdd = ((int) day - (int) start.DayOfWeek + 7) % 7;
            return start.AddDays(daysToAdd);
        }
        
        
    }
}