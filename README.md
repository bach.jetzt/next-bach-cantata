# Next Bach Cantata
This project is all about Johann Sebastian Bach (1685-1750). The composer wrote cantatas for many protestant holidays. The Compendium is scientifically collected in the BWV (Bach-Werke-Verzeichnis).

What do we do:
* we have several Nuget packages
  + `LiturgicalHolidays`: Calculates if a given date is a (protestant) liturgical holiday (as all sundays are)
  + `BachCantatasMapper`: Maps a protestant holiday to a BachCantata class (containing the Name, Bwv and our favorite appropriate Spotify-Url)
* we have a small console app `nextCantataConsole` for testing purposes
* we are planning a Xamarin Android app

## Project state
* Project is in active development
* Contributions are welcome
* This project is developed explorative (for learning purposes)

