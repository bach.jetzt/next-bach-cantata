import {Component} from '@angular/core';
import {DayOfWeek} from './enums/day-of-week';
import {LiturgicalHolidaysService} from './services/liturgical-holidays/liturgical-holidays.service';
import {HolidayCantataMapperService} from './services/bach-cantatas-mapper/holiday-cantata-mapper.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'next-cantata-angular-app';

  public nextHoliday: Date;

  constructor(private liturgicalHolidays: LiturgicalHolidaysService, private holidayCantataMapper: HolidayCantataMapperService) {
    const today = new Date();
    console.log('today:');
    console.log(today);

    this.nextHoliday = this.GetNextHoliday(today);

    console.log('next holiday is: ' + this.nextHoliday);
  }

  // tslint:disable-next-line
  private static GetNextWeekday(startDay: Date, day: DayOfWeek): Date {
    // The (... + 7) % 7 ensures we end up with a value in the range [0, 6]
    const daysToAdd = (day.valueOf() - startDay.getDay() + 7) % 7;
    const result = new Date(startDay);
    result.setDate(result.getDate() + daysToAdd);
    return result;
  }

  private GetNextHoliday(startDay: Date): Date {
    const liturgicalHoliday = this.liturgicalHolidays.GetLiturgicalHoliday(startDay);
    if (liturgicalHoliday === null) {
      const result = new Date(startDay);
      result.setDate(result.getDate() + 1);
      return this.GetNextHoliday(result);
    }

    return startDay;
  }
}
