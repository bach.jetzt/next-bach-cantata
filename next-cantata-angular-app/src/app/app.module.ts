import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {iconsPathFactory, TUI_ICONS_PATH, TuiButtonModule, TuiModeModule, TuiRootModule} from '@taiga-ui/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CurrentCantataCardComponent} from './components/current-cantata-card/current-cantata-card.component';
import {TuiIslandModule} from '@taiga-ui/kit';

@NgModule({
  declarations: [
    AppComponent,
    CurrentCantataCardComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    TuiModeModule,
    TuiRootModule,
    BrowserAnimationsModule,
    TuiIslandModule,
    TuiButtonModule,
  ],
  providers: [
    {
      provide: TUI_ICONS_PATH,
      useValue: iconsPathFactory('assets/taiga-ui/icons/'),
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
