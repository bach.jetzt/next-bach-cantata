import {CantataStatus} from '../enums/cantata-status';

export class BachCantata {
  public Name: string = '';
  public Bwv: string = '';
  public SpotifyUrl?: URL;
  public Status: CantataStatus = CantataStatus.NotAvailable;
}
