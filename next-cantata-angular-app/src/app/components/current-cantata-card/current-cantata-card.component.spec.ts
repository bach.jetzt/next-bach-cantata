import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentCantataCardComponent } from './current-cantata-card.component';

describe('CurrentCantataCardComponent', () => {
  let component: CurrentCantataCardComponent;
  let fixture: ComponentFixture<CurrentCantataCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CurrentCantataCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentCantataCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
