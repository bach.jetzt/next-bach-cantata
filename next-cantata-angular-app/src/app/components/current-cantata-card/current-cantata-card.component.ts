import {Component, Input, OnInit} from '@angular/core';
import {LiturgicalHolidaysService} from '../../services/liturgical-holidays/liturgical-holidays.service';
import {LiturgicalHoliday} from '../../enums/liturgical-holiday';
import {HolidayCantataMapperService} from '../../services/bach-cantatas-mapper/holiday-cantata-mapper.service';
import {Observable} from 'rxjs';
import {BachCantata} from '../../classes/bach-cantata';

@Component({
  selector: 'app-current-cantata-card',
  templateUrl: './current-cantata-card.component.html',
  styleUrls: ['./current-cantata-card.component.scss'],
})
export class CurrentCantataCardComponent implements OnInit {

  // @ts-ignore
  @Input() public liturgicalHolidayDate: Date;
  // @ts-ignore
  public liturgicalHoliday: LiturgicalHoliday;
  // @ts-ignore
  public possibleCantatas$: Observable<BachCantata[]>;

  constructor(private liturgicalHolidays: LiturgicalHolidaysService, private bachCantatasMapper: HolidayCantataMapperService) {
  }

  ngOnInit(): void {
    if (!this.liturgicalHolidayDate) {
      throw new Error('Attribute \'liturgicalHolidayDate\' is required');
    }

    this.liturgicalHoliday = this.liturgicalHolidays.GetLiturgicalHoliday(this.liturgicalHolidayDate) as LiturgicalHoliday;
    this.possibleCantatas$ = this.bachCantatasMapper.GetCantatas(this.liturgicalHoliday);
  }

}
