/**
 * Not all Cantatas are still preserved and have a recording on spotify. However, they are documented.
 */
export enum CantataStatus {
  /**
   * Existing and available on spotify
   */
  Accessible,
  /**
   * Not preserved
   */
  NotAvailable,
  /**
   * Preserved only in fragments
   */
  Fragment,
  /**
   * Not preserved completely
   */
  Incomplete,
}
