import { TestBed } from '@angular/core/testing';

import { HolidayCantataMapperService } from './holiday-cantata-mapper.service';

describe('HolidayCantataMapperService', () => {
  let service: HolidayCantataMapperService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HolidayCantataMapperService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
