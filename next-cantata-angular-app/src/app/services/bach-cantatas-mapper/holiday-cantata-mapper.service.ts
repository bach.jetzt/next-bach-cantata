import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {LiturgicalHoliday} from '../../enums/liturgical-holiday';
import {BachCantata} from '../../classes/bach-cantata';
import {LiturgicalHolidaysList} from './liturgical-holidays-list';
import {Observable} from 'rxjs';
import {map, shareReplay, take, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class HolidayCantataMapperService {

  private readonly data: Observable<LiturgicalHolidaysList>;

  constructor(private httpClient: HttpClient) {
    this.data = this.httpClient.get<LiturgicalHolidaysList>('assets/bach-cantatas.json').pipe(
      tap(console.log),
      shareReplay(),
    );
  }

  GetCantatas(liturgicalHoliday: LiturgicalHoliday): Observable<BachCantata[]> {

    return this.data.pipe(
      take(1),
      map(results => {
        return results.hasOwnProperty(liturgicalHoliday) ? results[liturgicalHoliday] : [];
      }),
    );
  }
}
