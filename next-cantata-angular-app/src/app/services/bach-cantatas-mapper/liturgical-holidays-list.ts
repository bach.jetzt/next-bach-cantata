import {BachCantata} from '../../classes/bach-cantata';

export interface LiturgicalHolidaysList {
  [key: string]: BachCantata[];
}
