import { TestBed } from '@angular/core/testing';

import { LiturgicalHolidaysService } from './liturgical-holidays.service';

describe('LiturgicalHolidaysService', () => {
  let service: LiturgicalHolidaysService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LiturgicalHolidaysService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
