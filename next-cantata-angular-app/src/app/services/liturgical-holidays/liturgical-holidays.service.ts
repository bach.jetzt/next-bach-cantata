import {Injectable} from '@angular/core';
import {LiturgicalHoliday} from '../../enums/liturgical-holiday';

@Injectable({
  providedIn: 'root',
})
export class LiturgicalHolidaysService {

  public GetLiturgicalHoliday(day: Date): LiturgicalHoliday | null {

    // TODO implement full algorithm (see c# project)
    if (day.getDate() % 10) {
      return null;
    }
    return LiturgicalHoliday.Advent4;
  }
}
