﻿using System;

namespace BachJetzt
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var today = DateTime.Today;
            //var today = new DateTime(2020, 4, 20);
            Console.WriteLine(today);
            var nextSunday = GetNextWeekday(today, DayOfWeek.Sunday);
            Console.WriteLine(nextSunday);


            Console.Write("Today is: ");
            Console.WriteLine(LiturgicalHolidays.GetLiturgicalHoliday(today));

            var nextLiturgicalSunday = LiturgicalHolidays.GetLiturgicalHoliday(nextSunday);
            Console.Write("next Sunday is: ");
            Console.WriteLine(nextLiturgicalSunday);

            if (nextLiturgicalSunday == null) return;

            var holidayCantataMapper = new HolidayCantataMapper();
            var possibleCantatas = holidayCantataMapper.GetCantatas((LiturgicalHoliday) nextLiturgicalSunday);
            Console.WriteLine("Found cantatas: " + possibleCantatas?.Count);

            if (possibleCantatas == null) return;

            foreach (var possibleCantata in possibleCantatas)
            {
                Console.WriteLine(possibleCantata);
            }
        }

        private static DateTime GetNextWeekday(DateTime start, DayOfWeek day)
        {
            // The (... + 7) % 7 ensures we end up with a value in the range [0, 6]
            int daysToAdd = ((int) day - (int) start.DayOfWeek + 7) % 7;
            return start.AddDays(daysToAdd);
        }
    }
}